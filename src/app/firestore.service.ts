import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';

@Injectable({
  providedIn: 'root'
})
export class FirestoreService {

  constructor(private angularFirestore: AngularFirestore) { }

  saveData(cities) {
    return this.angularFirestore.doc('cities/temperature').set({cities});
  }

  getData() {
    return this.angularFirestore.doc('cities/temperature').valueChanges();
  }
}
