export interface Weather {
    name:string,
    image:string,
    temperature:number,
    humidity:number
}
