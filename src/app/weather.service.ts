import { WeatherRaw } from './interfaces/weather-raw';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Weather } from './interfaces/weather';
import { throwError, observable, Observable } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { Data } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class WeatherService {
  private URL = "https://api.openweathermap.org/data/2.5/weather?q=";
  private KEY = "dcf2383efcc69b53a232ba3601a9c5f2";
  private IMP = "units=metric";
  
  private lamdaURL = "https://kq6zm626h9.execute-api.us-east-1.amazonaws.com/beta";


  constructor(private http: HttpClient) { }

  searchWeatherData(cityName:string):Observable<Weather>{
    return this.http.get<WeatherRaw>(`${this.URL}${cityName}&APPID=${this.KEY}&${this.IMP}`).pipe(
      map(data => this.transformWeatherData(data)),
      catchError(this.handleError)
    )
  }

  predict(city) {
    return this.http.post(this.lamdaURL, JSON.stringify({
      temperature: city.temperature,
      humidity: city.humidity
    }));
  }

  private handleError(res:HttpErrorResponse){
    console.log(res.error);
    return throwError(res.error || 'Server Error');
  }

  private transformWeatherData(data:WeatherRaw):Weather{
    return{
      name:data.name,
      image:`http://api.openweathermap.org/img/w/${data.weather[0].icon}.png`,
      temperature:data.main.temp,
      humidity:data.main.humidity
    }
  }

}


