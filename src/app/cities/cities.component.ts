import { FirestoreService } from './../firestore.service';
import { AuthService } from './../auth.service';
import { Component, OnInit } from '@angular/core';
import { WeatherService } from '../weather.service';

@Component({
  selector: 'app-cities',
  templateUrl: './cities.component.html',
  styleUrls: ['./cities.component.css']
})
export class CitiesComponent implements OnInit {

  panelOpenState = false;

  cities = [
    {name:'London', temperature: null, humidity: null,image:null, result: null },
    {name:'Paris', temperature: null, humidity: null,image:null, result: null },
    {name:'Tel Aviv', temperature: null, humidity: null,image:null, result: null },
    {name:'Jerusalem', temperature: null, humidity: null,image:null, result: null },
    {name:'Berlin', temperature: null, humidity: null,image:null, result: null },
    {name:'Rome', temperature: null, humidity: null,image:null, result: null },
    {name:'Dubai', temperature: null, humidity: null,image:null, result: null },
    {name:'Athens', temperature: null, humidity: null,image:null, result: null }
  ];

  errorMessage:string; 


  constructor(public authService:AuthService, private weatherService:WeatherService, private firestoreService: FirestoreService) { }

  ngOnInit(): void {
    this.getData();
  }

  getData() {
    this.firestoreService.getData().subscribe((data:any) => {
      this.cities = data.cities;
    })
  }

  weatherData$;
  getCityWeather(city) {
    this.weatherData$ = this.weatherService.searchWeatherData(city.name).subscribe((data) =>
      {
        city.temperature = data.temperature;
        city.image = data.image;
        city.humidity = data.humidity;
      })
  }

  predict(city) {
    this.weatherService.predict(city).subscribe((result:any) => {
      if(result.result > 50){
        city.result = 'Will Rain';
      }
      else {
        city.result = 'Will NOT Rain';
      }
      }
    )
  }

  save() {
    this.firestoreService.saveData(this.cities).then(() => {
      console.log('saved');
    })
  }
}

  



