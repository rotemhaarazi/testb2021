// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
    firebaseConfig: {
    apiKey: "AIzaSyB8-_Oz3xjG7JwMGn805hYXX7hlUxd1PFc",
    authDomain: "testb2021-b1f6d.firebaseapp.com",
    projectId: "testb2021-b1f6d",
    storageBucket: "testb2021-b1f6d.appspot.com",
    messagingSenderId: "546079612809",
    appId: "1:546079612809:web:01641f7cdfa1c8f53d8b58"
  },
  production: false
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
